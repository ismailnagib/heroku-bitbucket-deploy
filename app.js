require('dotenv').config();
const express = require('express');

const app = express();

const { PORT } = process.env;

app.use('/', (req, res) => res.send(`Deployed on port ${PORT}`));
app.listen(PORT || 3000);